import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:money_expense/util/SizeConfig.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'model/pengeluaran_model.dart';

class TambahPengeluaran extends StatelessWidget {
  TextEditingController nama_controller = TextEditingController();
  TextEditingController tanggal_controller = TextEditingController();
  TextEditingController harga_controller = TextEditingController();
  @override
//   _TambahPengeluaranState createState() => _TambahPengeluaranState();
//
//
// }
//
// class _TambahPengeluaranState extends State<TambahPengeluaran> {
  @override
  Widget build(BuildContext context) {





    var title = ["Makanan","Internet","Edukasi","Hadiah","Transport","Belanja","Alat Rumah","Olahraga","Hiburan"];
    var svgAssets = [
      "assets/logo/uil_pizza-slice.svg",
      "assets/logo/uil_rss-alt.svg",
      "assets/logo/uil_book-open.svg",
      "assets/logo/uil_gift.svg",
      "assets/logo/uil_car-sideview.svg",
      "assets/logo/uil_shopping-cart.svg",
      "assets/logo/uil_home.svg",
      "assets/logo/uil_basketball.svg",
      "assets/logo/uil_clapper-board.svg",
    ];

    double bottom = MediaQuery.of(context).viewInsets.bottom;
    SizeConfig().init(context);
    return Scaffold(
            resizeToAvoidBottomInset: false,
            resizeToAvoidBottomPadding: false,
            body: ChangeNotifierProvider(
              create: (context) => FormTambah(),
              builder:(context, child)=> SingleChildScrollView(
                reverse: true,
                child: Padding(
                  padding: EdgeInsets.only(bottom: bottom),
                  child: Container(
                    color: Colors.white,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //Tombol Back
                        Container(
                          margin: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 7.2, left: SizeConfig.blockSizeHorizontal * 7.6),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).pop();
                              print("pressed");
                            },
                            child: SvgPicture.asset("assets/logo/uil_arrow-chevron.svg"),
                          ),
                        ),
                        //String title
                        Container(
                          margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 7.8, top: SizeConfig.blockSizeVertical * 3.3),
                          child: Text("Tambah Pengeluaran Baru", style: TextStyle(
                            fontFamily: "Open Sans",
                            fontWeight: FontWeight.w700,
                            fontSize: SizeConfig.blockSizeHorizontal * 5
                          ),),
                        ),
                        //input field nama
                        Container(
                          margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 7.8, top: SizeConfig.blockSizeVertical * 7.23, right: SizeConfig.blockSizeHorizontal * 7.8),
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Nama Pengeluaran",
                                style: TextStyle(
                                    fontFamily: "Open Sans",
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12,
                                    color: Color(0xff4f4f4f)),
                              ),
                              Container(
                                margin:
                                    EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2),
                                child: new TextField(
                                  controller: nama_controller,
                                  decoration: new InputDecoration(
                                      hintText: "Nasi Goreng",
                                      hintStyle: TextStyle(
                                          fontFamily: "Open Sans",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14),
                                      suffixIcon: UnconstrainedBox()
                                      //   padding: const EdgeInsets.all(8.0),
                                      //   child: SvgPicture.asset("assets/logo/uil_car-sideview.svg", width: SizeConfig.blockSizeHorizontal * 6, height: SizeConfig.blockSizeVertical * 3,),
                                      // )
                                      // errorText: v_kelas ? null : "Tidak Boleh Kosong"),
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        //input kategori
                  Container(
                            margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 7.8, top: SizeConfig.blockSizeVertical * 3.89, ),
                            alignment: Alignment.centerLeft,
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Kategori",
                                    style: TextStyle(
                                        fontFamily: "Open Sans",
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                        color: Color(0xff4f4f4f)),
                                  ),
                                  Consumer<FormTambah>(builder: (context, kategori, child)=>
                                    Container(
                                        margin: EdgeInsets.only(
                                            top: SizeConfig.blockSizeVertical * 2),
                                        child: Row(
                                          children: [
                                            Container(
                                              // margin: EdgeInsets.only(
                                              //     bottom:SizeConfig.blockSizeVertical * 2.8),
                                              child: CircleAvatar(
                                                radius: SizeConfig.blockSizeVertical * 3.5,
                                                backgroundColor: Color(0xFFF2F2F2),
                                                child: CircleAvatar(
                                                  backgroundColor: Color(0x00),
                                                  child: Consumer<FormTambah>(builder:(context,kategori,child)=>
                                                    SvgPicture.asset(
                                                      svgAssets[kategori.value],
                                                      color: Colors.teal,
                                                      width: SizeConfig.blockSizeHorizontal * 7,
                                                      height: SizeConfig.blockSizeVertical * 4,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Align(
                                                alignment: Alignment.topLeft,
                                                child: Container(
                                                  padding: EdgeInsets.only(
                                                    left: SizeConfig.blockSizeHorizontal * 4,
                                                    right: SizeConfig.blockSizeHorizontal * 4,
                                                    // bottom:
                                                    //     SizeConfig.blockSizeVertical * 2.8
                                                  ),
                                                  child: Row(
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Text(title[kategori.value], style: TextStyle(
                                                          fontFamily: "Open Sans",
                                                          fontWeight: FontWeight.w400,
                                                          fontSize: 14),
                                                      ),
                                                      Transform.scale(
                                                        scale: 0.7,
                                                        child: IconButton(
                                                          icon: CircleAvatar(
                                                            backgroundColor: Color(0xfff2f2f2),
                                                            radius: 20,
                                                            child: CircleAvatar(
                                                              backgroundColor: Colors.transparent,
                                                              child: SvgPicture.asset(
                                                                  "assets/logo/uil_arrow-chevron-reversed.svg",
                                                                  width: SizeConfig
                                                                      .blockSizeHorizontal *
                                                                      4.7,
                                                                  height: SizeConfig
                                                                      .blockSizeVertical *
                                                                      2.3,
                                                                  color: Color(0xff828282)),
                                                            ),
                                                          ),
                                                          onPressed: () {
                                                            print(context);
                                                            _settingModalBottomSheet(context, "Pilih Kategori");
                                                          },
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            )
                                          ],
                                        )),

                                  ),
                                ]),
                          ),


                        //input tanggal
                        Container(
                          margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 7.8, top: SizeConfig.blockSizeVertical * 3.89, right: SizeConfig.blockSizeHorizontal * 7.8),
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Tanggal",
                                style: TextStyle(
                                    fontFamily: "Open Sans",
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12,
                                    color: Color(0xff4f4f4f)),
                              ),
                              Consumer<FormTambah>(builder:(context, tanggal, child) =>Container(
                                  margin:
                                      EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2),
                                  child: new TextField(
                                    readOnly: true,
                                    controller: tanggal_controller,
                                    onTap: (){
                                      _selectDate(context);
                                    },

                                    // controller: cont_kelas,
                                    decoration: new InputDecoration(
                                        hintText: "Senin, 27 juli 2020",
                                        hintStyle: TextStyle(
                                            fontFamily: "Open Sans",
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14),
                                        suffixIcon: UnconstrainedBox(
                                          // padding: const EdgeInsets.all(13.0),
                                          child: SvgPicture.asset(
                                            "assets/logo/uil_calendar.svg",
                                            width: SizeConfig.blockSizeHorizontal * 6,
                                            height: SizeConfig.blockSizeVertical * 3,
                                          ),
                                        )
                                        // errorText: v_kelas ? null : "Tidak Boleh Kosong"),
                                        ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        //input harga
                        Container(
                          margin: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 7.8, top: SizeConfig.blockSizeVertical * 3.89, right: SizeConfig.blockSizeHorizontal * 7.8),
                          alignment: Alignment.centerLeft,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "harga",
                                style: TextStyle(
                                    fontFamily: "Open Sans",
                                    fontWeight: FontWeight.w600,
                                    fontSize: 12,
                                    color: Color(0xff4f4f4f)),
                              ),
                              Container(
                                margin:
                                    EdgeInsets.only(top: SizeConfig.blockSizeVertical * 2),
                                child: new TextField(
                                  controller: harga_controller,
                                  decoration: new InputDecoration(
                                      prefixIcon: Text("Rp."),
                                      // prefix: Text("test"),
                                      // hintText: "Senin, 27 juli 2020",
                                      prefixIconConstraints:
                                          BoxConstraints(minWidth: 0, minHeight: 0),
                                      hintStyle: TextStyle(
                                          fontFamily: "Open Sans",
                                          fontWeight: FontWeight.w400,
                                          fontSize: 14,
                                          color: Color(0xff4f4f4f)),
                                      suffixIcon: UnconstrainedBox(
                                          // padding: const EdgeInsets.all(13.0),
                                          )
                                      // errorText: v_kelas ? null : "Tidak Boleh Kosong"),
                                      ),
                                ),
                              ),
                              //round button
                              Consumer<FormTambah>(builder: (context, button, child) =>
                                Container(
                                  margin: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 3.89),
                                  // padding: EdgeInsets.only( right: SizeConfig.blockSizeHorizontal * 7.8),
                                  width: double.infinity,
                                  child: RaisedButton(
                                    padding: EdgeInsets.only(
                                        top: SizeConfig.blockSizeVertical,
                                        bottom: SizeConfig.blockSizeVertical),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(8.0),
                                        side: BorderSide(color: Colors.teal)),
                                    onPressed: () {
                                      var data = context.read<FormTambah>();
                                      Pengeluaran newPengeluaran = Pengeluaran(
                                          nama_controller.text,
                                          data.value,
                                          data.tanggal,
                                          int.parse(harga_controller.text)
                                      );
                                      Navigator.of(context).pop(newPengeluaran);
                                      // print(
                                      //   widget.nama_controller.text + " " +
                                      //     data.tanggal.toString() + " " +
                                      //       data.value.toString() + " "+
                                      //
                                      //     widget.harga_controller.text
                                      // );
                                    },
                                    color: Colors.teal,
                                    textColor: Colors.white,
                                    child: Text("Simpan",
                                        style: TextStyle(
                                            fontFamily: "Open Sans",
                                            fontWeight: FontWeight.w700,
                                            fontSize: 14)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
      // );

  }

  void _settingModalBottomSheet(BuildContext context, title) {
    showModalBottomSheet(
      // isScrollControlled: true,
      backgroundColor: Colors.transparent,
        context: context,
        builder: (contextp) {
          return Container(
            padding: EdgeInsets.all(0.0),
            child: Card(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(SizeConfig.blockSizeHorizontal * 3.3),
              topRight: Radius.circular(SizeConfig.blockSizeHorizontal * 3.3))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: Row(
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                            fontFamily: "Open Sans",
                            fontWeight: FontWeight.w600,
                            fontSize: 14
                          ),
                        ),
                        GestureDetector(

                          child: Icon(Icons.close, size: SizeConfig.blockSizeHorizontal * 4,),
                          onTap: () {                 Navigator.of(context).pop();
                          },
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    ),
                    margin: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal * 7.8, top: SizeConfig.blockSizeVertical * 3.3, right: SizeConfig.blockSizeHorizontal * 8.6),
                  ),

                Container(
                       margin: EdgeInsets.only(top:SizeConfig.blockSizeVertical * 3, bottom: SizeConfig.blockSizeVertical * 8.75),
                       child: Center(
                         child: GridView.builder(
                           physics: NeverScrollableScrollPhysics(),
                           shrinkWrap: true,
                           gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: 1.6,crossAxisCount: 3),

                           itemCount: 9,
                         itemBuilder: (contexts, int pos) => itemPengeluaran(context, pos) ,),
                       ),
                     ),

                ],
              ),
            ),
          );
        });
  }

  itemPengeluaran(BuildContext context, int pos){
    var title = ["Makanan","Internet","Edukasi","Hadiah","Transport","Belanja","Alat Rumah","Olahraga","Hiburan"];
    var svgAssets = [
      "assets/logo/uil_pizza-slice.svg",
      "assets/logo/uil_rss-alt.svg",
      "assets/logo/uil_book-open.svg",
      "assets/logo/uil_gift.svg",
      "assets/logo/uil_car-sideview.svg",
      "assets/logo/uil_shopping-cart.svg",
      "assets/logo/uil_home.svg",
      "assets/logo/uil_basketball.svg",
      "assets/logo/uil_clapper-board.svg",
    ];
    return Center(
      child: Container(
        // height: SizeConfig.blockSizeVertical * 7,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          // crossAxisAlignment: WrapCrossAlignment.center,
          // alignment: WrapAlignment.center,
          children: [
            GestureDetector(
              onTap: (){
                var kategori = context.read<FormTambah>();
                kategori.change(pos);
                Navigator.of(context).pop();
              },
              // margin : EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal * 5, 0, 0, SizeConfig.blockSizeVertical * 3),
              child: CircleAvatar(
                radius: SizeConfig.blockSizeVertical*2.5,
                backgroundColor: Color(0xFFF2F2F2),
                child: CircleAvatar(
                  backgroundColor: Color(0x00),
                  child: SvgPicture.asset(svgAssets[pos],
                    color: Colors.teal,
                    width: SizeConfig.blockSizeHorizontal*5,
                    height: SizeConfig.blockSizeVertical*3,
                  ),
                ),
              ),
            ),
            Text(title[pos], style: TextStyle(
              fontFamily: "Open Sans",
              fontWeight: FontWeight.w400,
              color: Color(0xff828282)
            ),)
          ],
        ),
      ),
    );
  }

  Future _selectDate(BuildContext context) async {
    initializeDateFormatting('id_ID',null);
    var kategori = context.read<FormTambah>();
    // var provider = Provider.of<FormTambah>(context);
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.fromMillisecondsSinceEpoch(
            int.parse(kategori.tanggal.toString())),
        firstDate: new DateTime(2016),
        lastDate: new DateTime(2080)
    );
    if(picked != null) {
      kategori.changeDate(picked.millisecondsSinceEpoch);
     tanggal_controller.text =
         DateFormat('EEEEE, dd MMMM yyyy').format(
             DateTime.fromMillisecondsSinceEpoch(
                 int.parse(kategori.tanggal.toString())))
     ;}


  }



}

class FormTambah with ChangeNotifier {
  int value = 0;
  int tanggal = DateTime.now().millisecondsSinceEpoch;

  void change(val) {
    value = val;
    notifyListeners();
  }

  void changeDate(val){
    tanggal = val;
    notifyListeners();
  }
}
