import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:money_expense/tambah_pengeluaran.dart';
import 'package:money_expense/util/SizeConfig.dart';
import 'package:money_expense/util/database_helper.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import 'model/pengeluaran_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final title = ["Makanan","Internet","Edukasi","Hadiah","Transport","Belanja","Alat Rumah","Olahraga","Hiburan"];
  final svgAssets = [
  "assets/logo/uil_pizza-slice.svg",
  "assets/logo/uil_rss-alt.svg",
  "assets/logo/uil_book-open.svg",
  "assets/logo/uil_gift.svg",
  "assets/logo/uil_car-sideview.svg",
  "assets/logo/uil_shopping-cart.svg",
  "assets/logo/uil_home.svg",
  "assets/logo/uil_basketball.svg",
  "assets/logo/uil_clapper-board.svg",];

  @override
  Widget build(BuildContext context) {
    return
      ChangeNotifierProvider<DataMain>(
          create: (context) => DataMain(),
    builder:(context, child) =>
      MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    ),
      );
  }
}

class MyHomePage extends StatefulWidget {
  final dbHelper = DatabaseHelper.instance;
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}



class _MyHomePageState extends State<MyHomePage> {
  var grey2 = 0xff4f4f4f;
  var grey3 = 0xff828282;


  SizeConfig sizeConfig = SizeConfig();

  Size size;

  @override
  void initState() {
    super.initState();
    _queryinit();

  }

  @override
  Widget build(BuildContext context) {
    // sizeConfig.init(context);


    final nf = new NumberFormat("#,##0", "en_US");
    SizeConfig().init(context);
    size = MediaQuery.of(context).size;
    print(size.width*MediaQuery.of(context).devicePixelRatio);
    return Scaffold(
        backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        // appBar: AppBar(
        //   bottom: PreferredSize(
        //     preferredSize: Size.fromHeight(size.height * 0.12),
        //     child: Container(
        //       child: SizedBox(
        //       ),
        //     ),
        //   )
        // ),
        body:

              SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    //top section
                    Container(
                      color: Colors.teal,
                      height: SizeConfig.blockSizeVertical * 30,
                      width: double.infinity,

                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20, SizeConfig.blockSizeVertical*6, 0, 0),
                            child: CircleAvatar(
                              backgroundColor: Color(0x20333333),
                              radius: 32,
                              child: CircleAvatar(
                                child: SvgPicture.asset("assets/images/person.svg",width: 25, height: 25,),
                                backgroundColor: Color(0x00000000),
                              ),


                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(20, SizeConfig.blockSizeVertical * 2, 0, 0),
                            child: Text("Pengeluaran anda hari ini",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                                fontFamily: "Open Sans",
                                fontWeight: FontWeight.w300

                            ),),
                          ),
                          Consumer<DataMain>(builder: (context,harian,child)=>
                            Container(
                              alignment: Alignment.centerLeft,
                                margin: EdgeInsets.fromLTRB(20, SizeConfig.blockSizeVertical*0.8, 0, 0),
                              child: Text("Rp. "+nf.format(context.watch<DataMain>().totalharian),
                                  style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 24,
                                    fontFamily: "Open Sans",
                                    fontWeight: FontWeight.w700

                              )

                              )
                            ),
                          )
                        ],

                      ),
                    ),
                    //carousel section
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.fromLTRB(20, SizeConfig.blockSizeVertical * 3, 0, 0),
                      child: Column(
                        children: [
                          Text("Pengeluaran berdasarkan kategori",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontFamily: "Open Sans",
                            color: Color(grey2)
                          ),)
                        ],
                      ),
                    ),

                    Consumer<DataMain>(builder: (context,totalPengeluaran,child) =>
                      Container(
                        width: double.infinity,
                        height: SizeConfig.blockSizeVertical * 20,
                        margin: EdgeInsets.fromLTRB(0, SizeConfig.blockSizeVertical * 4, 0, 0),
                      child: ListView.builder(
                            itemBuilder: (BuildContext context, int pos)=>pageKategori(context, pos),
                          itemCount: 9,
                          scrollDirection: Axis.horizontal,

                        ),
                      ),
                    ),

                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(top:20, left:SizeConfig.blockSizeVertical * 3, bottom: SizeConfig.blockSizeVertical * 3.4),
                      child: Column(
                        children: [
                          Text("Semua Pengeluaran",
                            style: TextStyle(
                                fontWeight: FontWeight.w700,
                                fontFamily: "Open Sans",
                                color: Color(grey2)
                            ),)
                        ],
                      ),
                    ),


                      // ),
                 Container(
                     margin: EdgeInsets.only(left :SizeConfig.blockSizeHorizontal * 4, right: SizeConfig.blockSizeHorizontal * 4 ),
                     child: Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, SizeConfig.blockSizeHorizontal * 4, 0, 0),
                                    child: Text("Hari ini",style: TextStyle(
                                      fontFamily: "Open Sans",
                                      fontWeight: FontWeight.w700,
                                      color: Color(grey2)
                                    ),)),

                   Consumer<DataMain>(
                     builder: (context, pengeluaran, child) =>
                         Container(
                                    margin: EdgeInsets.only(top: SizeConfig.blockSizeVertical*3),
                                    child: ListView.builder(
                                        physics: NeverScrollableScrollPhysics(),
                                        padding: EdgeInsets.all(0.0),
                                        shrinkWrap: true,
                                        itemCount: !context.watch<DataMain>().pengeluaranData.length.isNaN ? context.watch<DataMain>().pengeluaranData.length : 1,
                                          itemBuilder: (contextp, int pos)=>
                                          context.read<DataMain>().pengeluaranData.isNotEmpty?
                                            itemPengeluaran(context, context.read<DataMain>().pengeluaranData[pos]) :
                                              Text("Tidak ada data")

                                      ),
                                  ),
                   ),
                                SizedBox(
                                  height: SizeConfig.blockSizeVertical * 1.2,
                                )


                              ],
                            ),
                          ),
                   ),

                    // ),
                    SizedBox(
                      height: SizeConfig.blockSizeVertical * 7,
                    )


                  ],

            ),
              ),
        floatingActionButton: FloatingActionButton(

          onPressed: (){
            showTambahPengeluaran();
          },
          child: Icon(Icons.add),
        ),
      );
  }

  Future showTambahPengeluaran() async {
    // push a new route like you did in the last section
    Pengeluaran pengeluaran = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) {
          return TambahPengeluaran();
        },
      ),
    );
    // A null check, to make sure that the user didn't abandon the form.
    if (pengeluaran != null) {
      Map<String, dynamic> row = {
        DatabaseHelper.kolomNama : pengeluaran.nama,
        DatabaseHelper.kolomNominal  : pengeluaran.nominal,
        DatabaseHelper.kolomKategori  : pengeluaran.kategori,
        DatabaseHelper.kolomTanggal  : pengeluaran.tanggal,

      };
      final id = await widget.dbHelper.insert(row);
      print('inserted row id: $id');
      _query();
    }
  }

  void _query() async {
    final allRows = await widget.dbHelper.queryLatest5();
    print('query all rows:');
    allRows.forEach((row) => print(row));
    Provider.of<DataMain>(context, listen:false).changePengeluaranData(allRows);
    Provider.of<DataMain>(context, listen:false).updateTotalKategori(allRows);
  }

  void _queryinit() async {
    final allRows = await widget.dbHelper.queryLatest5();
    print('query all rows:');
    // DataMain().changePengeluaranData(allRows);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<DataMain>(context, listen:false).changePengeluaranData(allRows);
      Provider.of<DataMain>(context, listen:false).updateTotalKategori(allRows);
    });

  }

  pageKategori(BuildContext context, int pos) {
    // double height = size.height * 0.17;
    // print(pos);
    final nf = new NumberFormat("#,##0", "en_US");
    final title = ["Makanan","Internet","Edukasi","Hadiah","Transport","Belanja","Alat Rumah","Olahraga","Hiburan"];
    final svgAssets = [
      "assets/logo/uil_pizza-slice.svg",
      "assets/logo/uil_rss-alt.svg",
      "assets/logo/uil_book-open.svg",
      "assets/logo/uil_gift.svg",
      "assets/logo/uil_car-sideview.svg",
      "assets/logo/uil_shopping-cart.svg",
      "assets/logo/uil_home.svg",
      "assets/logo/uil_basketball.svg",
      "assets/logo/uil_clapper-board.svg",];
    print(context
        .read<DataMain>()
        .pengeluaranTotal[pos]);
    // if(context.read<DataMain>().pengeluaranTotal==null) {
    //   // var total = 0;
    //   print(total);
    // }else{
    //   var total = context
    //       .read<DataMain>()
    //       .pengeluaranTotal[pos];
    // }
    return Container(
      margin: EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal * 5, 0, 0, 0),
      child: SizedBox(
        width : SizeConfig.blockSizeHorizontal *35,
        height: SizeConfig.blockSizeVertical * 20,
        child: Container(
          child: Card(
            elevation: 5,
            shape : RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(SizeConfig.blockSizeHorizontal * 3)
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              // direction: Axis.vertical,
              children: [
                Container(
                  margin : EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal * 5, SizeConfig.blockSizeVertical*3, 0, 0),
                  child: CircleAvatar(
                    radius: SizeConfig.blockSizeVertical*2.5,
                    backgroundColor: Colors.teal,
                    child: CircleAvatar(
                      backgroundColor: Color(0x00),
                      child: SvgPicture.asset(svgAssets[pos],
                      color: Colors.white,
                        width: SizeConfig.blockSizeHorizontal*5,
                        height: SizeConfig.blockSizeVertical*3,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin : EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal * 5, SizeConfig.blockSizeVertical*1.67, 0, 0),
                  child: Text(title[pos], style: TextStyle(
                    fontFamily: "Open Sans",
                    fontWeight: FontWeight.w400,
                    color: Color(0xff828282),
                    fontSize: SizeConfig.blockSizeHorizontal*3
                  ),),
                ),
                Container(
                  margin : EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal * 5, SizeConfig.blockSizeVertical*0.97, 0, 0),
                  child: Text("Rp. "+nf.format(context
                      .read<DataMain>()
                      .pengeluaranTotal[pos]), style: TextStyle(
                      fontFamily: "Open Sans",
                      fontWeight: FontWeight.w700,
                      fontSize: SizeConfig.blockSizeHorizontal*3,
                  color: Color(grey2))
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  itemPengeluaran(context, Pengeluaran pengeluaran){
    final nf = new NumberFormat("#,##0", "en_US");
    print(context);
    final svgAssets = [
      "assets/logo/uil_pizza-slice.svg",
      "assets/logo/uil_rss-alt.svg",
      "assets/logo/uil_book-open.svg",
      "assets/logo/uil_gift.svg",
      "assets/logo/uil_car-sideview.svg",
      "assets/logo/uil_shopping-cart.svg",
      "assets/logo/uil_home.svg",
      "assets/logo/uil_basketball.svg",
      "assets/logo/uil_clapper-board.svg",];
    return Row(
      children: [
        Container(
          margin : EdgeInsets.fromLTRB(SizeConfig.blockSizeHorizontal * 5, 0, 0, SizeConfig.blockSizeVertical * 2.8),
          child: CircleAvatar(
            radius: SizeConfig.blockSizeVertical*2.5,
            backgroundColor: Color(0xFFF2F2F2),
            child: CircleAvatar(
              backgroundColor: Color(0x00),
              child: SvgPicture.asset(svgAssets[pengeluaran.kategori],
                color: Colors.teal,
                width: SizeConfig.blockSizeHorizontal*5,
                height: SizeConfig.blockSizeVertical*3,
              ),
            ),
          ),
        ),
              Expanded(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    padding: EdgeInsets.only(left:SizeConfig.blockSizeHorizontal * 4, right: SizeConfig.blockSizeHorizontal*4, bottom: SizeConfig.blockSizeVertical * 2.8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(pengeluaran.nama, style: TextStyle(
                          fontFamily: "Open Sans",
                          fontWeight: FontWeight.w400,
                          fontSize: 12
                        ),),
                        Text("Rp. "+nf.format(pengeluaran.nominal), style: TextStyle(
                            fontFamily: "Open Sans",
                            fontWeight: FontWeight.w700,
                            fontSize: 12
                        ),)
                      ],
        ),
                  ),
                ),
              )
      ],
    );
  }
}

class DataMain extends ChangeNotifier{
  var totalharian = 0;
  List<Pengeluaran> pengeluaranData = new List<Pengeluaran>();
  var pengeluaranTotal = [0,0,0,0,0,0,0,0,0];
  
  void changePengeluaranData(var data){
    pengeluaranData.clear();
    data.forEach((row){
      var selisih = DateTime.now().subtract(Duration(milliseconds: row["tanggal"])).millisecondsSinceEpoch;
      if(selisih > 86400000 || selisih < 1){

      }else {
        pengeluaranData.add(new Pengeluaran(
            row["nama"], row["kategori"], row["tanggal"], row["nominal"]));
        totalharian += row["nominal"];
        print(row["nama"]);
      }
    });
    notifyListeners();

  }

  void updateTotalKategori(var data){
    pengeluaranTotal = [0,0,0,0,0,0,0,0,0];
    data.forEach((row){
      int kat = row["kategori"];
      pengeluaranTotal[kat] += row["nominal"];
      print(kat.toString() +" "+row["nominal"].toString());
    });
    print(pengeluaranTotal);
    print("totalharian"+totalharian.toString());
    notifyListeners();

  }
}
